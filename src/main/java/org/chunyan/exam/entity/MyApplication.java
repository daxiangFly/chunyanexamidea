package org.chunyan.exam.entity;

import lombok.Data;

@Data
public class MyApplication {
    private String runPath;
    private String appName;
}
