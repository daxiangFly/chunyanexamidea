package org.chunyan.exam.entity;

import lombok.Data;

/**
 * @program: chunyanexamnumber
 * @Description: 考生考试电脑
 * @author: Mr.Cheng
 * @Email: ccelephant_518@126.com
 * @date: 2024/4/1 12:40
 */
@Data
public class ExamComputer {
    private String computerId;
    private String computerName;
    private String computerIp;
}
