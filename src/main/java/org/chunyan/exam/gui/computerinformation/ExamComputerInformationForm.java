/*
 * Created by JFormDesigner on Mon Apr 01 13:21:34 CST 2024
 */

package org.chunyan.exam.gui.computerinformation;

import java.awt.event.*;

import org.chunyan.exam.gui.setting.SettingForm;
import org.chunyan.exam.gui.setting.SettingFromShowThread;
import org.chunyan.exam.service.network.NetWorkService;
import org.chunyan.exam.service.network.NetWorkServiceImpl;

import java.awt.*;
import java.io.*;
import java.net.SocketException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import javax.swing.*;

/**
 * @author chengchen
 */
public class ExamComputerInformationForm extends JFrame {
    NetWorkService netWorkService = new NetWorkServiceImpl();
    public ExamComputerInformationForm() {
        initComponents();
        setFrameIcon();
        setPromptMessage();
        setIPAddressLabelByLocation();
        setLocalNumberLabelByIP();
    }

    private void setPromptMessage(){
        String path = "exam.properties";
        File file = new File(path);
        try (Reader reader = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8)) {
            Properties prop = new Properties();
            prop.load(reader);
            String content =prop.getProperty("prompt");
            if(content!= null &&!content.isEmpty()){
                this.MessageLabel.setText(content);
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }

    }

    public JLabel getMessageLabel() {
        return MessageLabel;
    }

    private void setIPAddressLabelByLocation() {
        String ip = getLocalIPAddress();
        if(ip != null){
            this.IPAddressLabel.setText(ip);
        }
    }

    private void setLocalNumberLabelByIP() {
        String ip = getLocalIPAddress();
        if(ip != null){
            String[] numbers = ip.split("\\.",4);
            this.numberLabel.setText(numbers[3]);
        }
    }

    private String getLocalIPAddress() {
        String ip = null;
        try {
            ip = netWorkService.getLocalIp4Address().get().toString().replaceAll("/","");
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        return ip;
    }

    private void setFrameIcon(){
        URL url = ExamComputerInformationForm.class.getResource("/image/xx-b.png");
        ImageIcon icon = new ImageIcon(url);
        this.setIconImage(icon.getImage());
        logoLabel.setIcon(icon);
    }

    private void exitFrom(ActionEvent e) {
        // TODO add your code here
        int result = JOptionPane.showConfirmDialog(this,"确认退出程序？","退出程序",JOptionPane.YES_NO_OPTION);
        if(result == JOptionPane.YES_OPTION){
            System.exit(0);
        }
        if(result == JOptionPane.NO_OPTION){

        }
    }

    private void setting(ActionEvent e) {
        // TODO add your code here
        SettingFromShowThread thread = new SettingFromShowThread(this);
        EventQueue.invokeLater(thread);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        ResourceBundle bundle = ResourceBundle.getBundle("stringproperties.string");
        menuBar1 = new JMenuBar();
        menu1 = new JMenu();
        settingMenuItem = new JMenuItem();
        exitMenuItem = new JMenuItem();
        IPAddressLabel = new JLabel();
        IPShowLabel = new JLabel();
        localNumberLabel = new JLabel();
        numberLabel = new JLabel();
        logoLabel = new JLabel();
        MessageLabel = new JLabel();

        //======== this ========
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("\u7fd4\u8c61\u8003\u8bd5\u5411\u5bfc");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== menuBar1 ========
        {

            //======== menu1 ========
            {
                menu1.setText(bundle.getString("menu1.text"));

                //---- settingMenuItem ----
                settingMenuItem.setText(bundle.getString("settingMenuItem.text"));
                settingMenuItem.addActionListener(e -> setting(e));
                menu1.add(settingMenuItem);

                //---- exitMenuItem ----
                exitMenuItem.setText(bundle.getString("exitMenuItem.text"));
                exitMenuItem.addActionListener(e -> exitFrom(e));
                menu1.add(exitMenuItem);
            }
            menuBar1.add(menu1);
        }
        setJMenuBar(menuBar1);

        //---- IPAddressLabel ----
        IPAddressLabel.setFont(new Font("Arial Black", Font.BOLD, 12));
        IPAddressLabel.setHorizontalTextPosition(SwingConstants.CENTER);
        IPAddressLabel.setText(bundle.getString("IPAddressLabel.text"));
        contentPane.add(IPAddressLabel);
        IPAddressLabel.setBounds(330, 290, 120, 25);

        //---- IPShowLabel ----
        IPShowLabel.setText(bundle.getString("IPShowLabel.text"));
        IPShowLabel.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 12));
        contentPane.add(IPShowLabel);
        IPShowLabel.setBounds(245, 290, 80, 25);

        //---- localNumberLabel ----
        localNumberLabel.setText(bundle.getString("localNumberLabel.text"));
        localNumberLabel.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
        contentPane.add(localNumberLabel);
        localNumberLabel.setBounds(95, 35, 95, 30);

        //---- numberLabel ----
        numberLabel.setText("1");
        numberLabel.setFont(new Font("Arial Black", Font.BOLD, 36));
        contentPane.add(numberLabel);
        numberLabel.setBounds(195, 75, 135, 60);

        //---- logoLabel ----
        logoLabel.setText(bundle.getString("logoLabel.text"));
        contentPane.add(logoLabel);
        logoLabel.setBounds(20, 215, 100, 100);

        //---- MessageLabel ----
        MessageLabel.setText("\u5e7f\u5dde\u5546\u5b66\u9662\u73b0\u4ee3\u4fe1\u606f\u4ea7\u4e1a\u5b66\u9662\u9884\u795d\u5404\u4f4d\u8003\u751f\u8003\u8bd5\u987a\u5229\uff01");
        contentPane.add(MessageLabel);
        MessageLabel.setBounds(75, 165, 350, 25);

        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        setSize(485, 385);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JMenuBar menuBar1;
    private JMenu menu1;
    private JMenuItem settingMenuItem;
    private JMenuItem exitMenuItem;
    private JLabel IPAddressLabel;
    private JLabel IPShowLabel;
    private JLabel localNumberLabel;
    private JLabel numberLabel;
    private JLabel logoLabel;
    private JLabel MessageLabel;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
