package org.chunyan.exam.gui.setting;

import javax.swing.*;

public class SettingFromShowThread implements Runnable{

    private JFrame jFrame;

    public SettingFromShowThread(JFrame jFrame) {
        this.jFrame = jFrame;
    }

    @Override
    public void run() {
        SettingForm settingForm = new SettingForm(jFrame);
        settingForm.setVisible(true);
    }
}
