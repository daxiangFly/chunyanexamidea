package org.chunyan.exam.service.examcomputer;

/**
 * @program: chunyanexamnumber
 * @Description:
 * @author: Mr.Cheng
 * @Email: ccelephant_518@126.com
 * @date: 2024/4/1 12:42
 */
public interface ExamComputerService {
    boolean isStartupEnabled();
    boolean enableComputerStartup(String path);

    boolean disableComputerStartup();

    String getApplicationPath();
}
