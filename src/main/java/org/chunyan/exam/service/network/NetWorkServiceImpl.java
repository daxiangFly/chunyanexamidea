package org.chunyan.exam.service.network;

import org.apache.commons.lang3.ObjectUtils;

import java.net.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;

/**
 * @program: chunyanexamnumber
 * @Description:
 * @author: Mr.Cheng
 * @Email: ccelephant_518@126.com
 * @date: 2024/4/1 12:56
 */
public class NetWorkServiceImpl implements NetWorkService {
    /**
     * @return List<NetworkInterface>
     * @Description: 获取本机所有网卡信息   得到所有IP信息
     */
    @Override
    public List<Inet4Address> getLocalIp4AddressFromNetworkInterface() throws SocketException {
        List<Inet4Address> addresses = new ArrayList<>(1);

        // 所有网络接口信息
        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
        if (ObjectUtils.isEmpty(networkInterfaces)) {
            return addresses;
        }
        while (networkInterfaces.hasMoreElements()) {
            NetworkInterface networkInterface = networkInterfaces.nextElement();
            //滤回环网卡、点对点网卡、非活动网卡、虚拟网卡并要求网卡名字是eth或ens开头
            if (!isValidInterface(networkInterface)) {
                continue;
            }

            // 所有网络接口的IP地址信息
            Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
            while (inetAddresses.hasMoreElements()) {
                InetAddress inetAddress = inetAddresses.nextElement();
                // 判断是否是IPv4，并且内网地址并过滤回环地址.
                if (isValidAddress(inetAddress)) {
                    addresses.add((Inet4Address) inetAddress);
                }
            }
        }
        return addresses;
    }

    /**
     * @param networkInterface
     * @return List<NetworkInterface>
     * @Description: 过滤回环网卡、点对点网卡、非活动网卡、虚拟网卡并要求网卡名字是eth或ens开头的网卡
     */
    @Override
    public boolean isValidInterface(NetworkInterface networkInterface) throws SocketException {
        return !networkInterface.isLoopback() && !networkInterface.isPointToPoint() && networkInterface.isUp() && !networkInterface.isVirtual()
                && (networkInterface.getName().startsWith("eth") || networkInterface.getName().startsWith("ens"));
    }

    /**
     * @param inetAddress
     * @return boolean
     * @Description: 判断是否是IPv4，并且内网地址并过滤回环地址.
     */
    @Override
    public boolean isValidAddress(InetAddress inetAddress) {
        return inetAddress instanceof Inet4Address && inetAddress.isSiteLocalAddress() && !inetAddress.isLoopbackAddress();
    }

    /**
     * @return Optional<Inet4Address>
     * @Description: 通过Socket 唯一确定一个IP地址
     * 当有多个网卡的时候，使用这种方式一般都可以得到想要的IP。甚至不要求外网地址8.8.8.8是可连通的
     */
    @Override
    public Optional<Inet4Address> getIpBySocket() throws SocketException {
        try (final DatagramSocket socket = new DatagramSocket()) {
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            if (socket.getLocalAddress() instanceof Inet4Address) {
                return Optional.of((Inet4Address) socket.getLocalAddress());
            }
        } catch (UnknownHostException networkInterfaces) {
            throw new RuntimeException(networkInterfaces);
        }
        return Optional.empty();
    }

    /**
     * @return Optional<Inet4Address>
     * @Description: 获取本地IPv4地址
     */
    @Override
    public Optional<Inet4Address> getLocalIp4Address() throws SocketException {
        final List<Inet4Address> inet4Addresses = getLocalIp4AddressFromNetworkInterface();
        if (inet4Addresses.size() != 1) {
            final Optional<Inet4Address> ipBySocketOpt = getIpBySocket();
            if (ipBySocketOpt.isPresent()) {
                return ipBySocketOpt;
            } else {
                return inet4Addresses.isEmpty() ? Optional.empty() : Optional.of(inet4Addresses.get(0));
            }
        }
        return Optional.of(inet4Addresses.get(0));
    }
}
