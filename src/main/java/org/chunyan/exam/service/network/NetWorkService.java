package org.chunyan.exam.service.network;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.List;
import java.util.Optional;

/**
 * @program: chunyanexamnumber
 * @Description: 本地网络服务
 * @author: Mr.Cheng
 * @date: 2024/4/1 12:46
 */
public interface NetWorkService {
    /**
     * @Description: 获取本机所有网卡信息   得到所有IP信息
     * @return List<NetworkInterface>
     */
    List<Inet4Address> getLocalIp4AddressFromNetworkInterface() throws SocketException;

    /**
     * @Description: 过滤回环网卡、点对点网卡、非活动网卡、虚拟网卡并要求网卡名字是eth或ens开头的网卡
     * @return List<NetworkInterface>
     */
    boolean isValidInterface(NetworkInterface networkInterface) throws SocketException;

    /**
     * @Description: 判断是否是IPv4，并且内网地址并过滤回环地址.
     * @param inetAddress
     * @return boolean
     */
    boolean isValidAddress(InetAddress inetAddress);

    /**
     * @Description: 通过Socket 唯一确定一个IP地址
     * 当有多个网卡的时候，使用这种方式一般都可以得到想要的IP。甚至不要求外网地址8.8.8.8是可连通的
     * @return Optional<Inet4Address>
     */
    Optional<Inet4Address> getIpBySocket() throws SocketException;

    /**
     * @Description: 获取本地IPv4地址
     * @return Optional<Inet4Address>
     */
    Optional<Inet4Address> getLocalIp4Address() throws SocketException;
}
