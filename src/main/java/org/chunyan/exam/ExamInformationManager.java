package org.chunyan.exam;

import cn.hutool.core.lang.Singleton;
import org.chunyan.exam.entity.MyApplication;
import org.chunyan.exam.gui.computerinformation.ExamComputerInformationForm;

import javax.swing.*;
import java.net.URISyntaxException;

public class ExamInformationManager {
    public static void main(String[] args) {
        MyApplication application = Singleton.get(MyApplication.class);
        application.setRunPath(getApplicationPath());

        SwingUtilities.invokeLater(() -> {
            ExamComputerInformationForm examComputerInformationForm = new ExamComputerInformationForm();
            examComputerInformationForm.setVisible(true);
        });
    }

    private static String getApplicationPath() {
        try {
            String path = ExamInformationManager.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            if (System.getProperty("os.name").toLowerCase().contains("win")) {
                // Windows系统路径修正
                if (path.startsWith("/")) {
                    path = path.substring(1);
                }
            }
            return "\"" + path + "\""; // 返回路径，确保使用双引号包围，支持路径中的空格
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return ""; // 或返回一个错误信息
        }
    }
}