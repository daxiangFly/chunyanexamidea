package org.chunyan.exam.service.network;

import org.junit.Test;

import java.net.SocketException;

import static org.junit.Assert.*;

public class NetWorkServiceImplTest {

    NetWorkService netWorkService = new NetWorkServiceImpl();
    @Test
    public void getLocalIp4Address() {
        String ip = getLocalIPAddress();
        System.out.println(ip);
        if(ip != null){
            String[] numbers = ip.split("\\.",4);
            for (String number:numbers) {
                System.out.println(number);
            }
        }
    }

    private String getLocalIPAddress() {
        String ip = null;
        try {
            ip = netWorkService.getLocalIp4Address().get().toString().replaceAll("/","");
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        return ip;
    }
}