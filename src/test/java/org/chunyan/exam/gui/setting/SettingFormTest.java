package org.chunyan.exam.gui.setting;

import org.junit.Test;

import javax.swing.*;
import java.io.*;
import java.util.Properties;

public class SettingFormTest {

    @Test
    public void saveProperties() {
       // String path = this.getClass().getResource("/properties/exam.properties").getPath();
        String path = "exam.properties";
        File file = new File(path);
        try (OutputStream writer = new FileOutputStream(file)) {
            Properties prop = new Properties();
            prop.load(new FileInputStream(path));
            prop.setProperty("prompt", "111222");
            prop.store(writer, "exam.properties");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}